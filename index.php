<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("Shaun");

    echo "Animal Name : $sheep->name <br>";
    echo "Animal Legs : $sheep->legs <br>";
    echo "Cold Blooded Animal : $sheep->cold_blooded <br>";
    echo "<br>";

    $kodok = new Frog("Buduk");

    echo "Animal Name : $kodok->name <br>";
    echo "Animal Legs : $kodok->legs <br>";
    echo "Cold Blooded Animal : $kodok->cold_blooded <br>";
    $kodok->jump();
    echo "<br> <br>";

    $sungokong = new Ape("Kera Sakti");

    echo "Animal Name : $sungokong->name <br>";
    echo "Animal Legs : $sungokong->legs <br>";
    echo "Cold Blooded Animal : $sungokong->cold_blooded <br>";
    $sungokong->yell();

?>